﻿﻿# InkSoft Training Repository
### A little taste of InkSoft's .NET stack

This repository is intended to be a little bit of exposure to the main parts of InkSoft .NET stack - C#, SQL Server, ASP.NET MVC, and Linq to Sql.
The following tasks are typical of issues that come up at InkSoft.

## Setup and Prerequisites
This solution is a Visual Studio 2019 solution, although it should work in previous versions as well. If you don't have Visual Studio, 
there is a free trial available, or you can contact InkSoft and we'll make arrangements for you to use InkSoft equipment. Visual Studio 2019 
should have C#, ASP.NET and (optionally) LINQ to SQL tools installed.

You will need access to a SQL Server instance, any recent version will work. SQL Server Express should work, which is available [here](https://www.microsoft.com/en-us/sql-server/sql-server-editions-express)

The file DBSeed.sql creates a test database and seeds it with data. Run this script on your test database instance.

After you have the database created and seeded, adjust the connection string information in app.config to point to your SQL Server.

Finally, run the project and hit /api/cart/listcarts in the browser. You should get a list of 5 carts in JSON format.

Make sure you have the visual studio output window open when running the solution, it should give information about some of the issues we've planted in the solution.

## Problems and Questions
1. If you received a bug indicating that CartController.ListCartItems is too slow, how would you improve it? Create a pull request with the solution(s) *you* like best and explain what appeals to you about them.
1. If you received a bug indicating that CartController.ListCarts is too slow, how would you improve it? Create a pull request with the solution(s) *you* like best and explain what appeals to you about them.
1. There is a bug in CartController.ListCartItems, sometimes it returns a back image in `FrontImageUrl`. Create a pull request to fix this issue so it always returns the front image url, or null if it doesn't exist.

## Submission
To submit your solutions, first fork this repository into a new repository (it doesn't have to be on bitbucket, feel free to use your github or gitlab account if you prefer). Then create pull requests to your new repository 
with the changes, as well as any comments you want to make about what solutions you considered and why you chose the one you did. Finally, let your interviewer at InkSoft know when you're ready for us to review your code changes.